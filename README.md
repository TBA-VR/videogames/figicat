# Digicat The Friend

Video game for binocular treatment of amblyopia based on wavelength multiplexing division thecnique (anaglyph in computer science)


## Based on Digicat The Thief

You are a cat. A thief Cat. You steal diamonds. But diamonds are protected by other cats. You should avoid those cats. They say cats have 9 lives. They are wrong. You have 3. Is there an end? Not sure, but there is maybe in level 33. I am experimenting something. Thanks for playing.

code:  deniz aydinoglu

music: [eric skiff](http://ericskiff.com/music/)

playable version: http://deniz.itch.io/digicat-the-thief

## Authors

code: Yenner Diaz <ydiazn@udg.co.cu>
designs: Yenner Diaz <ydiazn@udg.co.cu>

## about code

This little game is built using awesome javascript game framework called [Phaser](http://phaser.io). Have fun hacking it as you wish. 

# How run

Open index.html in web browser

## Requirements
 - Firefox web browser with webGL support
 - Graphic processor with shader support
